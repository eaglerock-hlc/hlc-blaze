# Container for the HLC main webpage (hlc.eagleworld.net)

FROM eaglerock/eagleweb-template:v1.0
MAINTAINER Peter Marks <peter.marks@gmail.com>
USER root

# Prepare the webroot from the hlc-blaze GitLab repo
COPY html/ /usr/share/nginx/html/

# Expose port 80 and start web server
EXPOSE 80
