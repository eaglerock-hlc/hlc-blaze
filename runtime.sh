#!/bin/sh

cd /usr/share/nginx
git clone https://gitlab.com/eaglerock/hlc-blaze.git
rm -rf html
mv hlc-blaze html

nginx -g 'daemon off;'
